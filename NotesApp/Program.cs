﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using NotesApp.Models;

namespace NotesApp
{
    public class Program
    {
        public static NotesRepository NotesRepository = new NotesRepository
        {
            Notes = new List<Note>
            {
                new Note() { Created = DateTime.Now, Id = Guid.NewGuid(), Text = "New Note #1" },
                new Note() { Created = DateTime.Now, Id = Guid.NewGuid(), Text = "New Note #2" },
                new Note() { Created = DateTime.Now, Id = Guid.NewGuid(), Text = "New Note #3" },
            }
        };

        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
