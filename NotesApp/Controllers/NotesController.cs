﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NotesApp.Models;

namespace NotesApp.Controllers
{
    [Route("api/[controller]")]
    public class NotesController : Controller
    {
        // GET api/values
        [HttpGet]
        public IEnumerable<Note> Get()
        {
            return Program.NotesRepository.GetAll();
        }   

        // GET api/values/5
        [HttpGet("{id}")]
        public Note Get(Guid id)
        {
            return Program.NotesRepository.Get(id);
        }

        // POST api/values
        [HttpPost]
        public Note Post([FromBody]Note value)
        {
            return Program.NotesRepository.Create(value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public Note Put(Guid id, [FromBody]Note value)
        {
            return Program.NotesRepository.Update(id, value);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public bool Delete(Guid id)
        {
            return Program.NotesRepository.Delete(id);
        }
    }
}
