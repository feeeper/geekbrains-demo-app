﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesApp.Models
{
    public class Note
    {
        public Guid Id { get; set; }
        public DateTime Created { get; set; }
        public string Text { get; set; }
    }
}
