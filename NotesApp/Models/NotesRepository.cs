﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesApp.Models
{
    public class NotesRepository
    {
        public List<Note> Notes { get; set; }

        public List<Note> GetAll()
        {
            return Notes;
        }

        public Note Get(Guid id)
        {
            return Notes.FirstOrDefault(x => x.Id == id);
        }

        public Note Update(Guid id, Note note)
        {
            var existingNote = Notes.FirstOrDefault(x => x.Id == id);
            if (existingNote != null)
                existingNote.Text = note.Text;

            return existingNote;
        }

        public bool Delete(Guid id)
        {
            var existingNote = Notes.FirstOrDefault(x => x.Id == id);
            if (existingNote != null)
                return Notes.Remove(existingNote);

            return false;
        }

        public Note Create(Note value)
        {
            value.Id = Guid.NewGuid();
            value.Created = DateTime.Now;
            Notes.Add(value);
            return value;
        }
    }
}
